var http = require("http");
var server = http.createServer(httphandler);
var port = process.env.PORT || 8080;
const fs = require("fs"), url = require('url');

server.listen(port);
console.log("http server is running at port:" + port);

const querystring = require('querystring');
function httphandler(request, response) {
    console.log("Get an HTTP request:" + request.url);
    var fullpath = url.parse(request.url, true);
    var localfile = ".." + fullpath.pathname; // we serve files in the parent 
    console.log("Debug: Server's local file:" + localfile);

    if (fullpath.pathname === "/echo.php") {
        if (request.method == "GET") {
            // parsing the input from the query
            var query = querystring.parse(querystring.stringify(fullpath.query));
            response.writeHead(200, { 'Content-Type': 'text/html' });
            response.write(query.data);
            return response.end();
        }
        else if (request.method == "POST") {
            let postdata = '';
            //reading data from data stream
            request.on('data', (chunk) => {
                postdata += chunk;
            });
            request.on('end', () => {
                postdata = querystring.parse(postdata);
                response.writeHead(200, { 'Content-Type': 'text/html' });
                response.write(postdata.data);
                return response.end();
            });
        }
        return;
    }

    fs.readFile(localfile, (error, filecontent) => {
        if (error) {
            response.writeHead(404);
            return response.end(fullpath.pathname + " is not found on this server.");
        }
        response.writeHead(200, { 'Codntent-Type': 'text/html' });
        response.write(filecontent);
        return response.end();

    });


}
