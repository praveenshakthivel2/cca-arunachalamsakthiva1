 
const express = require('express');
const app = express();
var port = process.env.PORT || 8080; // important for deployment later
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const axios = require('axios');
const cors = require('cors')//New for microservice
const MongoClient = require('mongodb').MongoClient;
const mongourl = "mongodb+srv://cca-arunachalamsakthiva1:Mongopraveen7@cluster0.fjufa.mongodb.net/cca-labs?retryWrites=true&w=majority"
const dbClient = new MongoClient(mongourl, { useNewUrlParser: true, useUnifiedTopology: true });

server.listen(port); //changed from app.listen(port)
app.use(express.static('static'));
app.use(express.urlencoded({ extended: false }));
console.log("WebChat server is running on port " + port);


dbClient.connect(err => {
    if (err) throw err;
    console.log("Connected to MongoDB cluster");
});

app.get("/", (req, res) => {
    res.sendFile(__dirname + '/static/chatclient.html')
})

io.on('connection', (socketclient) => {
    var onlineClients = Object.keys(io.sockets.sockets).length;
    var welcomemessage = `${socketclient.id} is connected! Number of connected clients: ${onlineClients}`
    console.log(welcomemessage);
    io.emit("online", welcomemessage);
    console.log('A new client is connected!');
    // singup/login
    socketclient.on("message", (data) => {
        console.log('data from a client: ' + data);
        BroadcastAuthenticatedclient("message", `${socketclient.id} says: ${data}`);
    });
    socketclient.on('disconnect', () => {
        var onlineClients = Object.keys(io.sockets.sockets).length;
        var byemessage = `${socketclient.id} is disconnected! Number of connected clients: ${onlineClients}`
        console.log(byemessage);
        io.emit("online", byemessage);
    });
    socketclient.on("typing", () => {
        console.log('someone is typing...');
        io.emit("typing", `${socketclient.id} is typing ...`);
    });


    socketclient.on("loginuser", (username, password) => {

        let postData = {
            username: username,
            password: password
        }

        console.log(postData);
        axios.post("https://msauth.herokuapp.com/login", postData)
            .then(function (response) {
                console.log("Success");
                socketclient.authenticated = true;
                console.log(response.data);
                io.emit("login");
            })
            .catch(function (response) {
                console.log(response.data);
                io.emit("login-error", "User does not exist");
            });

    });

    socketclient.on("create", (fullname, email, username, password) => {
        console.log("Got inside the create");

        // creating a jSON object
        let postData = {
            username: username,
            password: password,
            fullname: fullname,
            email: email
        }

        console.log(postData);
        axios.post("https://msauth.herokuapp.com/signup", postData)
            .then(function (response) {
                console.log("Success");
                io.authenticated = true;
                io.username = postData.username;
                io.account = postData;
                io.emit("signup");
            })
            .catch(function (response) {
                console.log(response.data);
                io.emit("signup-error", "User already exist please login");
            });

    });

});
function BroadcastAuthenticatedclient(event,message){
    var sockets = io.sockets.sockets;
    for (var id in sockets){
        const socketclient = sockets[id];
        if(socketclient&&socketclient.authenticated){
            socketclient.emit(event,message)
        }
    }
}